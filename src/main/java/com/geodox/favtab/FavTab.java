package com.geodox.favtab;

import com.geodox.favtab.tab.FavCreativeTab;
import com.geodox.favtab.tab.data.FavouritesList;
import com.geodox.favtab.tab.render.TabOverlay;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

import static com.geodox.favtab.resources.ModInfo.*;

/**
 * Created by GeoDoX on 2016-01-31.
 */
@Mod(modid = MOD_ID, name = MOD_NAME, version = MOD_VERSION, clientSideOnly = true)
public class FavTab
{
    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        MinecraftForge.EVENT_BUS.register(this);
        MinecraftForge.EVENT_BUS.register(new TabOverlay());

        //ClientCommandHandler.instance.registerCommand(new FavCommand());

        new FavCreativeTab();
        FavouritesList.init(event.getSuggestedConfigurationFile());
    }
}
