package com.geodox.favtab.resources;

/**
 * Created by GeoDoX on 2016-01-31.
 */
public class ModInfo
{
    public static final String MC_VERSION = "1.8.9";
    public static final String FORGE_VERSION = "11.15.1.1765";

    public static final String MOD_ID = "@MODID@";
    public static final String MOD_NAME = "@MODNAME@";
    public static final String MOD_VERSION = "@MODVERSION@";
}
