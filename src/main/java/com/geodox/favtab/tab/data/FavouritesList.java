package com.geodox.favtab.tab.data;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.*;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by GeoDoX on 2016-01-31.
 */
public class FavouritesList
{
    private static FavouritesList instance;

    private final Configuration favouritesFile;
    private final Property favouritesProperty;

    private final List<ItemStack> favourites;

    public static void init(File favouritesFile)
    {
        if(instance == null)
        {
            instance = new FavouritesList(favouritesFile);
            instance.loadFavourites();
        }
    }

    private FavouritesList(File favouritesFile)
    {
        favourites = new ArrayList<ItemStack>();
        this.favouritesFile = new Configuration(favouritesFile);
        favouritesProperty = this.favouritesFile.get("favourites", "items", new String[0]);
    }

    public boolean addFavourite(ItemStack itemStack)
    {
        if(!isFavourite(itemStack))
        {
            favourites.add(itemStack);

            saveFavourites();
            return true;
        }

        return false;
    }

    public boolean removeFavourite(ItemStack itemStack)
    {
        if(isFavourite(itemStack))
        {
            int index = getIndexOfItemStack(itemStack);

            if(index != -1)
                favourites.remove(index);

            saveFavourites();
            return true;
        }

        return false;
    }

    public boolean addFavourite(String registeredItemName)
    {
        return Item.itemRegistry.containsKey(new ResourceLocation(registeredItemName)) && addFavourite(new ItemStack(Item.itemRegistry.getObject(new ResourceLocation(registeredItemName))));
    }

    public boolean removeFavourite(String registeredItemName)
    {
        return Item.itemRegistry.containsKey(new ResourceLocation(registeredItemName)) && removeFavourite(new ItemStack(Item.itemRegistry.getObject(new ResourceLocation(registeredItemName))));
    }

    public void saveFavourites()
    {
        ItemStack tempFavouriteStack;
        String tempFavouriteAsString;

        String[] favourites = new String[this.favourites.size()];

        for(int i = 0; i < this.favourites.size(); i++)
        {
            tempFavouriteStack = this.favourites.get(i);

            tempFavouriteAsString = "" + Item.itemRegistry.getNameForObject(tempFavouriteStack.getItem()).toString();

            int favMeta = tempFavouriteStack.getMetadata();
            if(favMeta != 0)
                tempFavouriteAsString += "@" + favMeta;

            if(tempFavouriteStack.getTagCompound() != null)
                tempFavouriteAsString += tempFavouriteStack.getTagCompound().toString();

            favourites[i] = tempFavouriteAsString;
        }

        favouritesProperty.set(favourites);
        favouritesFile.save();
    }

    public void loadFavourites()
    {
        favourites.clear();

        String tempFavouriteName;
        String tempFavouriteMeta;
        String tempFavouriteTag;

        int indexOfMetaStart;
        int indexOfTagStart;

        for(String tempFavouriteAsString : favouritesProperty.getStringList())
        {
            tempFavouriteName = "";
            tempFavouriteMeta = null;
            tempFavouriteTag = null;

            indexOfMetaStart = tempFavouriteAsString.indexOf('@');

            if(indexOfMetaStart > 0)
            {
                tempFavouriteName = tempFavouriteAsString.substring(0, indexOfMetaStart);

                tempFavouriteAsString = tempFavouriteAsString.substring(indexOfMetaStart+1);
            }

            indexOfTagStart = tempFavouriteAsString.indexOf('{');

            if (indexOfTagStart > 0)
            {
                tempFavouriteMeta = tempFavouriteAsString.substring(0, indexOfTagStart);
                tempFavouriteTag = tempFavouriteAsString.substring(indexOfTagStart);
            }
            else if (indexOfTagStart == 0)
            {
                tempFavouriteTag = tempFavouriteAsString;
            }
            else
            {
                tempFavouriteMeta = tempFavouriteAsString;
            }

            if(tempFavouriteName.equals(""))
                tempFavouriteName = tempFavouriteAsString;

            Item item = Item.itemRegistry.getObject(new ResourceLocation(tempFavouriteName));
            if(item == null)
                continue;

            int meta = 0;
            if(tempFavouriteMeta != null && tempFavouriteMeta.length() > 0)
                meta = Integer.parseInt(tempFavouriteMeta);

            NBTTagCompound tag = null;
            if(tempFavouriteTag != null)
            {
                try
                {
                    tag = JsonToNBT.getTagFromJson(tempFavouriteTag);
                }
                catch (NBTException ignored) { }
            }


            ItemStack favStack = new ItemStack(item, 1, meta);
            if(tag != null)
                favStack.setTagCompound(tag);

            this.favourites.add(favStack);
        }
    }

    public boolean isFavourite(ItemStack itemStack)
    {
        for (ItemStack i : favourites)
        {
            if(ItemStack.areItemsEqual(i, itemStack) && ItemStack.areItemStackTagsEqual(i, itemStack))
                return true;
        }

        return false;
    }

    private int getIndexOfItemStack(ItemStack itemStack)
    {
        for (ItemStack i : favourites)
        {
            if(ItemStack.areItemsEqual(i, itemStack) && ItemStack.areItemStackTagsEqual(i, itemStack))
                return favourites.indexOf(i);
        }

        return -1;
    }

    public List<ItemStack> getFavourites()
    {
        return favourites;
    }

    public static FavouritesList getInstance()
    {
        return instance;
    }
}
